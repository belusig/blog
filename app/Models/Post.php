<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\File;

class Post{
    public $title;
    public $excerpt;
    public $date;
    public $body;

    public function __construct($title, $excerpt, $date, $body){
        $this->title = $title;
        $this->excerpt = $excerpt;
        $this->date = $date;
        $this->body = $body;
    }


    public static function all(){
        $files = File::files(resource_path("posts/"));

        return array_map(fn($file) => $file->getContents(), $files);
    }
    
    public static function find($sludge){
        $path = resource_path("posts/{$sludge}.html");
    if(!file_exists($path)){
        // abort(404);
        throw new ModelNotFoundException();
    }
    return cache()->remember("post.{$sludge}", 5, fn() => file_get_contents($path));
    }
}