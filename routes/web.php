<?php

use Illuminate\Support\Facades\Route;
use App\Models\Post;
use Spatie\YamlFrontMatter\YamlFrontMatter;
use Illuminate\Support\Facades\File;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $files = File::files(resource_path("posts"));
    $posts = [];

    foreach($files as $file){
        $document[] = YamlFrontMatter::parseFile($file);
        $posts[] = new Post(
            $document[0]->title,
            $document[0]->excerpt,
            $document[0]->date,
            $document[0]->body()
        );
    }

    //{ asdas
    // $document = YamlFrontMatter::parseFile(
    //     resource_path('posts/first-post.html')
    // );

    // ddd($document->body());
    // ddd($document->title);
    // ddd($document->excerpt);
    //}

    return view('posts', ['posts' => $document]);

    // $post = Post::all();

    // ddd($post);
    // ddd($post[0]->getPathname());
    // ddd($post[0]->getContents());

    // return view('posts', [
    //     'posts' => $post
    // ]);
    // return Post::find('first-post');
});

Route::get("post/{post}", function($slug) {

    return view('post', [
        'post' => Post::find($slug)
    ]); 
})->where('post', '[A-z_\-]+');